const express = require("express");
const path = require("path");
const fs = require('fs');
// Get constant files. my-courses.json will be fetched in the functions since it is dynamic.
const subjects = require("./subjects.json");
const courses = require("./courses.json");
var bodyParser = require("body-parser");


// Skapa en instans av express
const app = express();

app.use(bodyParser.json());

var urlencodedParser = bodyParser.urlencoded({ extended: false })

// Skapa statisk sökväg
app.use(express.static(path.join(__dirname, 'public'))); // localhost:3000/public/index.html -> localhost:3000/index.html

// Port för anslutning
const port = process.env.PORT || 3000;

// Send filepath to views
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/index.html'));
});

app.get('/index.html', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/index.html'));
});

app.get('/my-courses.html', function (req, res) {
    res.sendFile(path.join(__dirname + '/views/my-courses.html'));
});

// Return courses including the subject for each course appended to each course element.
app.get("/api/courses", (req, res) => {
    let clientCourses = courses;  // clientCourses is the json object returned to the client. 
    clientCourses.courses.forEach(course => {
        let subject = subjects.subjects.find(subject => course.subjectCode === subject.subjectCode);
        if (subject !== undefined)
            course.subject = subject.subject;  // For each course find the subject name and append to the course.
    });
    res.status(200).send(JSON.stringify(clientCourses));
});

app.get("/api/my/courses", (req, res) => {
    let myCourses = JSON.parse(fs.readFileSync('./my-courses.json', 'utf-8'));  // require cannot be used here 
    // as a cached version might be returned that doesn't include the updated courses. 
    let clientMyCourses = myCourses.myCourses.map(myCourse => {
        let courseCompleted = myCourse.completed;
        myCourse = courses.courses.find(course => course.courseCode === myCourse.courseCode);
        myCourse.completed = courseCompleted;  // Include the subject name in myCourse element
        myCourse.subject = subjects.subjects.find(subject => myCourse.subjectCode === subject.subjectCode).subject;
        return myCourse;
    });
    let newJsonObject = { myCoursesInfo: myCourses.myCoursesInfo, myCourses: clientMyCourses };  // Create new object with updated course elements
    res.send(JSON.stringify(newJsonObject));  // returning JSON object
});

// Return course name. 
app.get("/api/my-course-name/:id", (req, res) => {
    courseCode = req.params.id;
    let course = courses.courses.find(course => course.courseCode === courseCode);
    if (course === undefined) {  
        res.status(400).send("could not retrieve course for given course code");
    } else {
        courseName = course.name;
        if (courseName !== undefined)
            res.status(200).send(courseName);
        else
            res.status(400).send("could not retrieve course name for given course code");
    }
});

// Find and return course from courses
app.get("/api/courses/:courseCode", (req, res) => {
    console.log("hello");
    let courseCode = req.params.courseCode;
    let course = courses.courses.find(course => course.courseCode === courseCode);
    if (course !== undefined) {
        let subject = subjects.subjects.find(subject => course.subjectCode === subject.subjectCode);
        course.subject = subject.subject;
        res.status(200).send(JSON.stringify(course));
    } else
        res.status(400).send("could not retrieve course for given course code");
});

// Find and return course from my-courses
app.get("/api/my/courses/:courseCode", (req, res) => {
    let myCourses = JSON.parse(fs.readFileSync('./my-courses.json', 'utf-8'));  // Could have been done asynchronously with readFile
    // however this solution experienced errors. A synchronous function was therefore used to read the file. 
    let courseCode = req.params.courseCode;
    let myCourse = myCourses.myCourses.find(myCourse => myCourse.courseCode === courseCode);
    let course = courses.courses.find(course => course.courseCode === courseCode);
    if (course !== undefined || myCourse !== undefined) {
        let subject = subjects.subjects.find(subject => course.subjectCode === subject.subjectCode);
        myCourse = course;
        myCourse.subject = subject.subject;
        res.status(200).send(myCourse);
    } else
        res.status(400).send("could not retrieve course for given course code");
});

// Return all subjects
app.get("/api/subjects", (req, res) => {
    res.status(200).send(JSON.stringify(subjects));
});

// Retrieve subject of given subjectcode
app.get("/api/subjects/:subjectCode", (req, res) => {
    let subjectCode = req.params.subjectCode;
    let subject = subjects.subjects.find(subject => subjectCode === subject.subjectCode);
    if (subject !== undefined) {
        res.status(200).send(subject);
    } else
        res.status(400).send("could not retrieve subject for subject code provided");
});

// Retrieve courses.json
app.get("/courses.json", (req, res) => {
    res.status(200).send(JSON.stringify(courses));
});

// Retrieve my-courses.json
app.get("/my-courses.json", (req, res) => {
    res.status(200).send(JSON.stringify(require("./my-courses.json")));
});

// Add course to my-courses.json
app.post("/api/my/courses/add", urlencodedParser, (req, res) => {
    let courseCode = req.body.courseCode;
    let completed = req.body.completed;
    let newElement = {
        courseCode: courseCode,
        completed: completed
    }
    let jsonObject;
    fs.readFile("./my-courses.json", 'utf-8', (err, jsonString) => {
        jsonObject = JSON.parse(jsonString); // Check if course already exists
        if ((jsonObject.myCourses.find(course => course.courseCode === courseCode)) !== undefined) {
            res.status(400).send("Course already exists");
        } else {
            if (courses.courses.find(course => course.courseCode === courseCode) === undefined) {
                res.status(400).send("course doesn't exist");  // Check if the course exists
            } else {
                jsonObject.myCourses.push(newElement);
                fs.writeFile("./my-courses.json", JSON.stringify(jsonObject, null, 2), err => {
                    if (err) {
                        console.log(err);
                        res.status(400).send("An error occured whilst writing to file")
                    } else {
                        res.status(200).send("File successfully written");
                    }
                });
            };
        };
    });
});

// Update completed value for given course
app.put("/api/my/courses/:courseCode", (req, res) => {
    let courseCode = req.params.courseCode;
    fs.readFile("./my-courses.json", 'utf-8', (err, jsonString) => {
        let jsonObject = JSON.parse(jsonString);
        let myCourse = jsonObject.myCourses.find(course => course.courseCode === courseCode);
        let completed;
        if (myCourse.completed === "oavklarad") {  // Switch completion indicator
            completed = "avklarad"
        } else {
            completed = "oavklarad"
        }
        jsonObject.myCourses.find(course => course.courseCode === courseCode).completed = completed;
        writeToFile(jsonObject, req, res);
    });
});

// Delete course from my-courses for given course
app.delete("/api/my/courses/delete/:courseCode", (req, res) => {
    let courseCode = req.params.courseCode;
    let jsonObject;
    fs.readFile("./my-courses.json", 'utf-8', (err, jsonString) => {
        jsonObject = JSON.parse(jsonString);
        for (let i = 0; i < jsonObject.myCourses.length; ++i) {
            if (jsonObject.myCourses[i].courseCode === courseCode) {
                jsonObject.myCourses.splice(i, 1);  // Remove onde element from index i
            }
        }
        writeToFile(jsonObject, req, res);
    });
});

// Start server
app.listen(port, function () {
    console.log("Server running on port " + port);
});

// Function to write updated json objects to my-courses.json for "/api/my/courses/add",
// "/api/my/courses/:courseCode" and "/api/my/courses/delete/:courseCode"
function writeToFile(jsonObject, req, res) {
    fs.writeFile("./my-courses.json", JSON.stringify(jsonObject, null, 2), err => {
        if (err) {
            console.log(err);
            res.status(400).send("An error occured whilst writing to file")
        } else {
            res.status(200).send("File successfully written");
        }
    });
}